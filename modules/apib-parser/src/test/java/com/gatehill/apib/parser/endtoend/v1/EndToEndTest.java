package com.gatehill.apib.parser.endtoend.v1;

import com.gatehill.apib.parser.model.AstFormat;
import com.gatehill.apib.parser.model.ParserConfiguration;
import com.gatehill.apib.parser.model.ParsingResult;
import com.gatehill.apib.parser.model.ast.v1.AstBlueprint;
import com.gatehill.apib.parser.model.result.AstResult;
import com.gatehill.apib.parser.service.BlueprintParserService;
import com.gatehill.apib.parser.service.SnowcrashBlueprintParserServiceImpl;
import com.gatehill.apib.parser.service.v1.JsonAstParserServiceImpl;
import com.gatehill.apib.parser.support.TestUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

/**
 * End to end tests from Markdown Blueprint, to AST V1, to model.
 * <p/>
 * The example Blueprints from <a href="https://raw.githubusercontent.com/apiaryio/api-blueprint/">Github</a> are
 * used as test data.
 *
 * @author pcornish
 */
public class EndToEndTest {
    private ParserConfiguration config;
    private BlueprintParserService blueprintParserService;
    private JsonAstParserServiceImpl astParserService;

    @Before
    public void before() {
        config = TestUtil.buildConfigFromEnvironmentVariable(TestUtil.ENV_SNOWCRASH1_HOME);

        blueprintParserService = new SnowcrashBlueprintParserServiceImpl();
        astParserService = new JsonAstParserServiceImpl();
    }

    @Test
    public void testExample1() {
        // test data
        final String blueprintFilename = "/examples/1.md";
        final String apiName = "The Simplest API";
        final int resourceGroupCount = 1;
        loadAndVerifyBlueprint(blueprintFilename, apiName, resourceGroupCount);
    }

    @Test
    public void testExample2() {
        // test data
        final String blueprintFilename = "/examples/2.md";
        final String apiName = "Resource and Actions API";
        final int resourceGroupCount = 1;
        loadAndVerifyBlueprint(blueprintFilename, apiName, resourceGroupCount);
    }

    @Test
    public void testExample3() {
        // test data
        final String blueprintFilename = "/examples/3.md";
        final String apiName = "Named Resource and Actions API";
        final int resourceGroupCount = 1;
        loadAndVerifyBlueprint(blueprintFilename, apiName, resourceGroupCount);
    }

    @Test
    public void testExample4() {
        // test data
        final String blueprintFilename = "/examples/4.md";
        final String apiName = "Grouping Resources API";
        final int resourceGroupCount = 2;
        loadAndVerifyBlueprint(blueprintFilename, apiName, resourceGroupCount);
    }

    @Test
    public void testExample5() {
        // test data
        final String blueprintFilename = "/examples/5.md";
        final String apiName = "Responses API";
        final int resourceGroupCount = 1;
        loadAndVerifyBlueprint(blueprintFilename, apiName, resourceGroupCount);
    }

    @Test
    public void testExample6() {
        // test data
        final String blueprintFilename = "/examples/6.md";
        final String apiName = "Requests API";
        final int resourceGroupCount = 1;
        loadAndVerifyBlueprint(blueprintFilename, apiName, resourceGroupCount);
    }

    @Test
    public void testExample7() {
        // test data
        final String blueprintFilename = "/examples/7.md";
        final String apiName = "Parameters API";
        final int resourceGroupCount = 1;
        loadAndVerifyBlueprint(blueprintFilename, apiName, resourceGroupCount);
    }

    @Test
    public void testExample8() {
        // test data
        final String blueprintFilename = "/examples/8.md";
        final String apiName = "Resource Model API";
        final int resourceGroupCount = 1;
        loadAndVerifyBlueprint(blueprintFilename, apiName, resourceGroupCount);
    }

    /**
     * Load the Markdown format Blueprint into AST form, then parse the AST into the model.
     *
     * @param blueprintFilename
     * @param apiName
     * @param resourceGroupCount
     */
    private void loadAndVerifyBlueprint(String blueprintFilename, String apiName, int resourceGroupCount) {
        final File blueprintFile = new File(EndToEndTest.class.getResource(blueprintFilename).getPath());

        // call
        final ParsingResult<File> actual = blueprintParserService.convertToAstFile(config, blueprintFile, AstFormat.JSON);

        // assert output
        Assert.assertNotNull(actual);

        final AstResult result = actual.getResult();
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getError());
        Assert.assertEquals(AstResult.ErrorCode.NoError.getErrorCode(), result.getError().getCode());
        Assert.assertNotNull(result.getWarnings());
        Assert.assertEquals(0, result.getWarnings().length);

        final File astFile = actual.getAst();
        Assert.assertNotNull(astFile);
        Assert.assertTrue(astFile.getAbsolutePath().endsWith(".json"));
        Assert.assertTrue("AST file exists", astFile.exists());

        // call
        final AstBlueprint astBlueprint = astParserService.fromAst(astFile, AstFormat.JSON);

        // assert
        Assert.assertNotNull(astBlueprint);
        Assert.assertEquals(apiName, astBlueprint.getName());
        Assert.assertEquals(resourceGroupCount, astBlueprint.getResourceGroups().size());
    }
}
