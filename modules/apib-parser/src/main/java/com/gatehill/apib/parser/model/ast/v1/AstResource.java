package com.gatehill.apib.parser.model.ast.v1;

import java.util.List;
import java.util.Map;

/**
 * @author pcornish
 */
public class AstResource extends AstBase {
    /**
     * URI template
     */
    private String uriTemplate;

    /**
     * Model representing this Resource
     */
    private AstResourceModel model;

    /**
     * Parameters
     */
    private Map<String, AstParameter> parameters;

    /**
     * Resource-specific HTTP Headers
     */
    private Map<String, AstValueType> headers;

    /**
     * A set of Actions specified for this Resource
     */
    private List<AstAction> actions;

    public String getUriTemplate() {
        return uriTemplate;
    }

    public void setUriTemplate(String uriTemplate) {
        this.uriTemplate = uriTemplate;
    }

    public AstResourceModel getModel() {
        return model;
    }

    public void setModel(AstResourceModel model) {
        this.model = model;
    }

    public Map<String, AstParameter> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, AstParameter> parameters) {
        this.parameters = parameters;
    }

    public Map<String, AstValueType> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, AstValueType> headers) {
        this.headers = headers;
    }

    public List<AstAction> getActions() {
        return actions;
    }

    public void setActions(List<AstAction> actions) {
        this.actions = actions;
    }
}
