package com.gatehill.apib.parser.model.ast.v1;

import java.util.List;

/**
 * @author pcornish
 */
public class AstResourceGroup extends AstBase {
    /**
     * Resources
     */
    private List<AstResource> resources;

    public List<AstResource> getResources() {
        return resources;
    }

    public void setResources(List<AstResource> resources) {
        this.resources = resources;
    }
}
