package com.gatehill.apib.parser.model.ast.v2;

import java.util.List;

/**
 * @author pcornish
 */
public class AstResource extends AstBase {
    /**
     * URI template
     */
    private String uriTemplate;

    /**
     * Model representing this Resource
     */
    private AstResourceModel model;

    /**
     * Parameters
     */
    private List<AstParameter> parameters;

    /**
     * Resource-specific HTTP Headers
     */
    private List<AstNameValueType> headers;

    /**
     * A set of Actions specified for this Resource
     */
    private List<AstAction> actions;

    public String getUriTemplate() {
        return uriTemplate;
    }

    public void setUriTemplate(String uriTemplate) {
        this.uriTemplate = uriTemplate;
    }

    public AstResourceModel getModel() {
        return model;
    }

    public void setModel(AstResourceModel model) {
        this.model = model;
    }

    public List<AstParameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<AstParameter> parameters) {
        this.parameters = parameters;
    }

    public List<AstNameValueType> getHeaders() {
        return headers;
    }

    public void setHeaders(List<AstNameValueType> headers) {
        this.headers = headers;
    }

    public List<AstAction> getActions() {
        return actions;
    }

    public void setActions(List<AstAction> actions) {
        this.actions = actions;
    }
}
