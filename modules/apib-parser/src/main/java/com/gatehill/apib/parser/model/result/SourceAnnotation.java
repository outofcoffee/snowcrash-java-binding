package com.gatehill.apib.parser.model.result;

/**
 * A source data annotation.
 * <p/>
 * Annotation bound to a source data block. Includes an annotation code and an optional message.
 * <p/>
 * See https://github.com/apiaryio/snowcrash/blob/master/src/SourceAnnotation.h
 *
 * @author pcornish
 */
public class SourceAnnotation {
    private int code;
    private String message;
    private SourceCharactersRange location;

    /**
     * @param code     - An annotation message.
     * @param message  - Annotation code.
     * @param location - A location of the annotation.
     */
    public SourceAnnotation(int code, String message, SourceCharactersRange location) {
        this.code = code;
        this.message = message;
        this.location = location;
    }

    @Override
    public String toString() {
        return "SourceAnnotation{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", location=" + location +
                '}';
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public SourceCharactersRange getLocation() {
        return location;
    }
}
