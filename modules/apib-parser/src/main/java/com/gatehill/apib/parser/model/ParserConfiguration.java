package com.gatehill.apib.parser.model;

import java.io.File;

/**
 * @author pcornish
 */
public class ParserConfiguration {
    private File parserHome;

    public File getParserHome() {
        return parserHome;
    }

    public void setParserHome(File parserHome) {
        this.parserHome = parserHome;
    }
}
